<?php 

/**
 * @file
 * configuration form for CSS
 * Implements hook_form().
 */
 
function addstyle_css_form($form_state) {
  $options = addstyle_css_files ();
  $addFiles = variable_get ( 'addstyle_css_files' );
  $rmFiles = variable_get ( 'addstyle_css_files_rm' );
  $addcss= (!empty($addFiles)) ? implode(",\n <br>", $addFiles) : 'No File Selected';
  $rmcss = (!empty($rmFiles)) ? implode(",\n <br>", $rmFiles) : 'No File Selected';
  
  $form ['addstyle_css_files'] = array (
      '#type' => 'select',
      '#title' => t ( 'Add CSS files to site' ),
      '#suffix' => t('<p><b>List of selected CSS files</b><p>') .  $addcss,
      '#options' => $options,
      '#default_value' => variable_get ( 'addstyle_css_files' ),
      '#description' => t ( '<i>List of available style</i>' ),
      '#multiple' => TRUE,
      '#size' => 15
  );
  
  $form ['addstyle_css_files_rm'] = array (
      '#type' => 'select',
      '#title' => t ( 'Remove CSS files from site' ),
      '#suffix' => t('<p><b>List of selected CSS files</b><p>') .  $rmcss,
      '#options' => $options,
      '#default_value' => variable_get ( 'addstyle_css_files_rm' ),
      '#description' => t ( '<i>List of avaible CSS</i>' ),
      '#multiple' => TRUE,
      '#size' => 15
  );
  
  $form['my_field'] = array(
      '#type' => 'horizontal_tabs',
      '#tree' => TRUE,
      '#prefix' => '<div id="unique-wrapper">',
      '#suffix' => '</div>',
  );
  
  return system_settings_form ( $form );
}


/**
 * Return availble CSS files list
 */
function addstyle_css_files() {
  $no_files =array ('None' => '--None--');
  $lib_files = array ();
  $module_css = array ();
  $pr_files = array ();
  
  $themeDefault = variable_get ( 'theme_default' );
  $installProfile = variable_get ( 'install_profile' ); // install profile
  
  $themeDir = drupal_get_path ( 'theme', $themeDefault );
  
  $themeFiles = file_scan_directory ( $themeDir, '/.*\.css$/' );
  
  $sitesFiles = file_scan_directory ( 'sites/all/', '/.*\.css$/' );
  
  $moduleList = module_list ();
  foreach ( $moduleList as $path ) {
    $moduleDir = drupal_get_path ( 'module', $path );
    $moduleFiles = file_scan_directory ( $moduleDir, '/.*\.css$/' );
    foreach ( $moduleFiles as $mpath ) {
      $module_css [$mpath->uri] = $mpath->uri;
    }
  }
  
  foreach ( $sitesFiles as $path ) {
    $type = explode ( "/", $path->uri );
    if ($type [3] != "modules" && $type [3] != "themes") {
      $lib_files [$path->uri] = $path->uri;
    }
  }
  
  $profileFiles = file_scan_directory ( 'profiles/' . $installProfile, '/.*\.css$/' );
  
  foreach ( $profileFiles as $ppath ) {
    $type = explode ( "/", $ppath->uri );
    if ($type [2] != "modules" && $type [2] != "themes") {
      $pr_files [$ppath->uri] = $ppath->uri;
    }
  }
  
  $css_files = $no_files + $module_css + $lib_files + $pr_files;
  
  return $css_files;
}

