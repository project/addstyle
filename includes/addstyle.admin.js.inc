<?php 


/**
 * @file
 * configuration form for JS
 * Implements hook_form().
 */


function addstyle_js_form($form_state) {
  $options = addstyle_js_files();
  $addFiles = variable_get ( 'addstyle_js_files' ); 
  $rmFiles = variable_get ( 'addstyle_js_files_rm' ); 
  $addjs = (!empty($addFiles)) ? implode(",\n <br>", $addFiles) : 'No File Selected';
  $rmjs = (!empty($rmFiles)) ? implode(",\n <br>", $rmFiles) : 'No File Selected';
  
  $form ['addstyle_js_files'] = array (
      '#type' => 'select',
      '#title' => t ( 'Add JS files to site' ),
      '#suffix' => t('<p><b>List of selected JS files</b><p>') .  $addjs,
      '#options' => $options,
      '#default_value' => variable_get ( 'addstyle_js_files' ),
      '#description' => t ( '<i>List of available JS files</i>' ),
      '#multiple' => TRUE,
      '#size' => 15 
  );
  
  $form ['addstyle_js_files_rm'] = array (
      '#type' => 'select',
      '#title' => t ( 'Remove JS files from site' ),
      '#suffix' => t('<p><b>List of selected JS files</b><p>') .  $rmjs,
      '#options' => $options,
      '#default_value' => variable_get ( 'addstyle_js_files_rm' ),
      '#description' => t ( '<i>List of avaible JS</i>' ),
      '#multiple' => TRUE,
      '#size' => 15
  );
  
  return system_settings_form ( $form );
}


/**
 * Return availble JS files list
 */
function addstyle_js_files() {
  $no_files = array ('None' => '--None--');
  $lib_files = array ();
  $module_js = array ();
  $pr_files = array ();
  
  $themeDefault = variable_get ( 'theme_default' );
  $installProfile = variable_get ( 'install_profile' ); // install profile
  $themeDir = drupal_get_path ( 'theme', $themeDefault );  
  $themeFiles = file_scan_directory ( $themeDir, '/.*\.js$/' );
  $sitesFiles = file_scan_directory ( 'sites/all/', '/.*\.js$/' );
  
  $moduleList = module_list ();
  foreach ( $moduleList as $path ) {
    $moduleDir = drupal_get_path ( 'module', $path );
    $moduleFiles = file_scan_directory ( $moduleDir, '/.*\.js$/' );
    foreach ( $moduleFiles as $mpath ) {
      $module_js [$mpath->uri] = $mpath->uri;
    }
  }
  
  foreach ( $sitesFiles as $path ) {
    $type = explode ( "/", $path->uri );
    if ($type [3] != "modules" && $type [3] != "themes") {
      $lib_files [$path->uri] = $path->uri;
    }
  }
  
  $profileFiles = file_scan_directory ( 'profiles/' . $installProfile, '/.*\.js$/' );
  
  foreach ( $profileFiles as $ppath ) {
    $type = explode ( "/", $ppath->uri );
    if ($type [2] != "modules" && $type [2] != "themes") {
      $pr_files [$ppath->uri] = $ppath->uri;
    }
  }
      
  $js_files = $no_files + $module_js + $lib_files + $pr_files;
  
  return $js_files;
}









